const express = require('express');
const app = express();
const port = 9090;

const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
const mongourl = "mongodb://localhost:27017";
const bodyParser = require('body-parser');

let db;
let col_name='emp_table';

app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())



app.get('/',(req,res) => {
    res.send("Hello Anand Bhagwat");
})

app.get('/employees',(req,res) => {
    db.collection(col_name).find({}).toArray((err,result) => {
        if(err) throw err;
        else{
            res.send(result)
        }
    })
})

app.post('/addemployees',(req,res) => {

    db.collection(col_name).insert(req.body,(err,result) =>{
        if(err) throw err;
        else{
            res.send('Data Inserted')
        } 
    })
})

app.delete('/deleteemployees',(req,res) => {

    db.collection(col_name).remove({"emp_id":req.body.emp_id},(err,result) =>{
        if(err) throw err;
        else{
            res.send('Data Deleted')
        } 
    })
})

app.put('/updateemployees',(req,res) => {
    db.collection(col_name).findOneAndUpdate({emp_id:req.body.emp_id},{
        $set:{
            "emp_id":req.body.emp_id,
            "emp_firstname":req.body.emp_firstname,
            "emp_lastname":req.body.emp_lastname,
            "emp_salary":req.body.emp_salary
        }
    },(err,result) =>{
        
        if(err) throw err;
        else{
            res.send('Data Updated')
        } 
    })
})

MongoClient.connect(mongourl,(err,client) =>{
    db = client.db('employeedatabase')
    app.listen(port,(err) => {
        console.log(`App is running on port ${port}`)
    })
})

